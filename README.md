## Ejemplo de aplicación Springboot con cache REDIS

Está la opción de utilizar una sola instancia de REDIS o un CLUSTER, revisar application.properties

El ejemplo con CLUSTER utiliza un [AKS como prerequisito](https://gitlab.com/sahc/containers/docker/redis-over-aks)

El pom tiene la tarea directa para generar la imagen Docker (dockerfile:build).

Se debe hacer login en el ACR del AKS de forma manual y despues el PUSH tambien se debe hacer en forma manual.

Por ultimo se debe crear el servicio en el AKS, primero configuramos el contexto:

``` bash
az aks get-credentials --resource-group POC-AKS-Terraform --name axityaks
```

Y despues creamos el deployment en al AKS, con el comando:

``` bash
kubectl apply -f redis-cache-demo.yml
```




