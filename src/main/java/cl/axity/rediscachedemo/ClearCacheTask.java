package cl.axity.rediscachedemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ClearCacheTask {
    @Autowired
    private CacheManager cacheManager;

    private final Logger LOG = LoggerFactory.getLogger(getClass());

    @Scheduled(fixedRate = 60000) // reset cache every 30 seconds
    public void reportCurrentTime() {
        LOG.info("Clearing cache");
        cacheManager.getCacheNames().parallelStream().forEach(name -> cacheManager.getCache(name).clear());
        cacheManager.getCacheNames().parallelStream().forEach(name -> LOG.info("Cache name: " + name));
    }
}
