package cl.axity.rediscachedemo;

public class RedisCacheException  extends RuntimeException {



        private static final long serialVersionUID = 1L;

        private String errorCode;
        private String errorMsg;

        public RedisCacheException(String code) {
            this.errorMsg = "";
            this.errorCode = code;
        }

        public RedisCacheException(String code, String v) {
            this.errorMsg = v;
            this.errorCode = code;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public String getErrorMsg() {
            return errorMsg;
        }
}
