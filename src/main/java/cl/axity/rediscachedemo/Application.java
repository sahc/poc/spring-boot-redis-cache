package cl.axity.rediscachedemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableCaching
@EnableScheduling
public class Application implements CommandLineRunner {

	private final Logger LOG = LoggerFactory.getLogger(getClass());

	private final UserRepository userRepository;

	@Autowired
	public Application(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... strings) {

		//Populating embedded database here
		LOG.info("Saving users. Current user count is {}.", userRepository.count());
		User shume = new User("Sergio", "Hume", 2000);
		User ceyhe = new User("Cristian", "Eyheramendy", 29000);
		User caro = new User("Carolina", "Cornejo", 550);

		userRepository.save(shume);
		userRepository.save(ceyhe);
		userRepository.save(caro);

		LOG.info("Done saving users. Data: {}.", userRepository.findAll());

		LOG.trace("Mensaje de trace");
		LOG.debug("Mensaje de debug");
		LOG.info("Mensaje de info");
		LOG.warn("Mensaje de warning");
		LOG.error("Error {} - Mensaje de error: {}", "1544", "Redis no disponible", new RedisCacheException("1544-Redis no disponible"));

	}
}
