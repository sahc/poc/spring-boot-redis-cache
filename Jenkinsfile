properties([[$class: 'GitLabConnectionProperty', gitLabConnection: 'gitlab']])
String app_name = "fidcl-redis-dev"
String img = "cencoreg:5000/fidcl/dev"
String gitrepo = "ssh://git@gitlab.cencosud.corp:29418/celula_Puntos_Cencosud_UN_Fidelidad_Chile/4051_Back_CacheRedis_Sitio_Web_Puntos_Cencosud.git"
String build_id = "${env.BUILD_ID}"

String nspace = 'fidcl'
String k_cloud = 'k8sqacl01'
String k_folder = 'dev'
String image_id = "redis-dev"
String image = "${img}:${image_id}"

podTemplate(
	cloud: k_cloud,
	label: 'fidcl',
	containers: [
		containerTemplate(name: 'docker', image: 'docker:git', idleMinutes: 30, slaveConnectTimeout: 3000, ttyEnabled: true, command: 'cat'),
        containerTemplate(name: 'maven', image: 'maven:3.5.3-jdk-8-alpine', idleMinutes: 30, slaveConnectTimeout: 3000, ttyEnabled: true, command: 'cat'),
        containerTemplate(name: 'kubectl', image: 'amaceog/kubectl', idleMinutes: 30, slaveConnectTimeout: 3000, ttyEnabled: true, command: 'cat')
	],
	volumes: [
		hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock'),
	]
){
	node('fidcl') {
		gitlabCommitStatus(name: 'jenkins') {
		    
		    stage('Redis') {
				cleanWs()
				git branch: "${env.BRANCH_NAME}", credentialsId: 'EC0011-SSH', url: "${gitrepo}"
				container('kubectl') {
					sh 'kubectl -n ${nspace} apply -f k8s/${k_folder}/redis.yml'
				}
			}
		    
		    
			stage('Build JAR') {
			    container('maven') {
					sh 'mvn --batch-mode -V clean package -Dmaven.test.skip=true'
					archiveArtifacts artifacts: 'target/redis-cache-demo-0.0.1-SNAPSHOT.jar', fingerprint: false
				}
			}
			
			stage('Test JAR') {
				container('maven') {
					sh 'mvn test'
					sh 'ls -lrt target/ '
				}
			}
			

			stage('Push JAR to Nexus') {
				withCredentials([usernamePassword(credentialsId: 'nexus-registry', passwordVariable: 'DOCKER_PASSWD', usernameVariable: 'DOCKER_USER')]) {
					container('docker') {
						sh 'docker build --no-cache --build-arg JAR_FILE=./target/redis-cache-demo-0.0.1-SNAPSHOT.jar -t ' + image + ' .'
						sh "echo ${DOCKER_PASSWD} |docker login -u ${DOCKER_USER} --password-stdin cencoreg:5000"
						sh "docker push ${image} && docker rmi ${image}"
					}
				}
			}
			
			stage('Deploy to Kubernetes'){
        		container('kubectl'){
		 			sh "kubectl -n ${nspace} apply -f k8s/${k_folder}/redis-cache-demo.yml"
        		}
      		}

      		stage('Running or Rollback'){
        		container('kubectl'){
          			sh 'sleep 60'
          			sh "kubectl -n ${nspace} rollout status deployment/${app_name} | tail -1 > deploymentStatus.txt"
          			String deploymentStatus = readFile('deploymentStatus.txt').trim()
          			echo "Deployment status is: ${deploymentStatus}"
					String statusCorrecto = "deployment \"${app_name}\" successfully rolled out"
					if (deploymentStatus == statusCorrecto) {
            			sh 'rm -rf deploymentStatus.txt'
            			echo 'Se ha creado el deploy correctamente'
          			} else {
            			sh "kubectl -n ${nspace} rollout undo deployment/${app_name}"
            			error 'El deploy fue cancelado, revise jenkinsfile para más información'
          			}
        		}
      		}
		}
	}
}
